\documentclass[11pt,twoside]{article}
\usepackage[utf8]{inputenc} % apparemment utf8x est mieux qu’utf8 mais c'est incompatible avec biblatex...
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{textcomp}
% \usepackage[style=numeric,backend=biber]{biblatex}
\usepackage{csquotes}
\usepackage[english]{babel}
\usepackage[pdftex]{hyperref}
\usepackage{verbatim}
\usepackage{alltt}
\usepackage{hhline}
\usepackage{lmodern}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage[a4paper,margin=2cm]{geometry}
% \usepackage[letterpaper,margin=2cm]{geometry}
% \usepackage[hmarginratio=1:1]{geometry}
\usepackage{listings}
\usepackage{float}
\usepackage{url}
\usepackage{tabularx}
\usepackage{calc}
\usepackage{color}
\usepackage{wasysym}
\usepackage{enumitem}

% \bibliography{labo}


\begin{document}

\title{A brief introduction to xAAL v0.7-draft rev 2\footnote{Compares to
"xAAL v0.7-draft rev 1b", this revision introduces the new specific target
address (UUID)(0) for \texttt{is\_alive} requests.}}

\author{Christophe Lohr \and Jérôme Kerdreux}

\date{}

\maketitle

\begin{abstract}
This document summary information which specifies xAAL such as defined by
the IHSEV/HAAL team of IMT-Atlantique.

% This release proposes to use CBOR in place of JSON for
% xAAL messages, and CDDL in schemas in place of Json Schema.
\end{abstract}


\section{Introduction}
The xAAL system is a solution for home automation interoperability.
Simply speaking, it allows a device from vendor A (e.g. a switch) to talk
to a device from vendor B (e.g. a lamp).

The xAAL specification defines: (i) a functional distributed architecture;
(ii) a means to describe and to discover the interface and the expected
behavior of participating nodes by the means of so-called schemas; and (iii)
a secure communications layer via an IP (multicast) bus.


\paragraph{The philosophy.}
xAAL is a distributed system based on ``the best effort'' principle.  Each one does its
best according to its capacity for things to go well.  There is no
warranty.  There is no quality requirement to expect/provide from/to
others.  

\paragraph{} Following papers discuss main points of xAAL:
\begin{enumerate}
\item C. Lohr, P. Tanguy, J. Kerdreux, ``xAAL: A Distributed Infrastructure for Heterogeneous Ambient Devices'', Journal of Intelligent Systems. Volume 24, Issue 3, Pages 321–331, ISSN (Online) 2191-026X, ISSN (Print) 0334-1860, DOI: 10.1515/jisys-2014-0144, March 2015.
\item C. Lohr, P. Tanguy, J. Kerdreux, ``Choosing security elements for the xAAL home automation system'', IEEE Proceedings of ATC 2016, pp.534 - 541, Jul 2016, Toulouse, France.
\item C. Lohr, J. Kerdreux, ``Improvements of the xAAL Home Automation System'', Future Internet 12, no. 6:104, https://doi.org/10.3390/fi12060104, 2020
\end{enumerate}


%                                      *
%                                     * *

\section{The xAAL Architecture}
The xAAL system is made of functional entities communicating to each other
via a messages bus over IP.  Each entity may have multiple instances or
zero, may be shared between several boxes, and may be physically located on
any box.

Figure \ref{archi} shows the general functional architecture of the xAAL
system in a typical home automation facility.

\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{bus-xaal}
\caption{Functional Architecture of xAAL.\label{archi}}
\end{figure}


\paragraph{Native Equipment.}
  Some home automation devices (sensors, actuators) can communicate natively
  using the xAAL protocol.

\paragraph{Gateways.}
  In general, devices for home automation only support their own proprietary
  communication protocol. 
  Therefore, elementary \emph{gateways} have the responsibility to translate
  messages between the manufacturer protocols and the xAAL protocol.

  Within an installation, each manufacturer protocol may be served by a
  dedicated gateway.

  According to the technologies and the manufacturer protocols, each gateway
  will handle the following issues: pairing between the gateway and physical
  devices, addresses of devices, configuration, and the persistence of the
  configuration.

  Gateways can be queried about the list of devices they manage.

\paragraph{Database of Metadatas.}
  Each automation device within a facility is likely to be associated with a
  piece of user-defined information: for instance some location information
  (e.g.  declare that the equipment typed as a lamp and having address X is
  located in the ``kitchen'' or in the ``bedroom of Bob''), possibly a
  friendly name to be displayed to the end user (e.g.  device having address
  X is called ``lamp\#1''), or any useful piece of information for users
  regarding devices on its facility (e.g.  pronunciation of the nickname of
  the device for a voice interface, or whatever).

  All this information is grouped in a \emph{database of metadata}.
  This database contains somehow the configuration of home automation
  devices, from the end-user point of view.

  The database of metadata is present on the xAAL bus.  Other xAAL devices
  can query it via this bus to obtain information associated with the
  identifier of a device, or conversely a list of devices associated with a
  given piece of information.

  There should be at least one database of metadata on the bus.  There could
  be several.
  
  Such pieces of information are structured as key-value pairs.  Keys and
  values are UTF8 strings.  The xAAL specification does not define a
  normative list of pre-defined or well-known key entries.  As a
  consequence, the meaning of a key makes sense only for the entity that
  write it in the database and for the entities which read it.  This is the
  responsibility of those entities to agree on a common semantics to
  interpret information.

\paragraph{Cache.}
  Unidirectional sensors are quite common: one cannot question them, they
  send their information sporadically.  (e.g.  A thermometer which sends the
  temperature only if it changes.)

  So, there should be at least one cache on the xAAL bus that stores this
  information so that other entities can query it whenever necessary.  Note
  that even if such caching feature is implemented by the gateway software
  itself, the cache is seen as a dedicated xAAL device.

  Such a cache should store at least the values of attributes carried by
  notifications sent by the devices.  It can also stay informed by
  monitoring request/reply messages between devices about values of
  attributes.

  As with any caching mechanism, it is necessary to associate a timestamp to
  cached information.  When another entity on the xAAL bus asks the cache
  for information, it also gets the age of the cached information, and
  decides itself if it is good enough or not.  This is not the
  responsibility of the cache to manage the relevance of data according to
  their age.  This is the responsibility of the client that makes the
  request.  Moreover, the clock used by caches to set such timestamps may
  not be accurate or synchronized well.  This is the responsibility of
  clients to deal with that.

  Again, inconsistencies may arise if two caches return information that has
  the same timestamp but divergent values.  This phenomenon is a priori very
  rare.  However, the xAAL specification does not enforce any rule to solve
  inconsistencies.

\paragraph{Automata of Scenarios.} 
  Scenarios are advanced home automation services like, for example start a
  whole sequence of actions from a click of the user, or at scheduled times,
  or monitor sequences of events and then react, etc.

  To do this, xAAL proposes to support it by one or more entities of the
  type \emph{automata of scenarios}.

  These automata of scenarios are also the right place to implement virtual
  devices. For example, consider a scenario to check for the presence of
  users in a room: it could aggregate and correlate a variety of
  events from real devices, and then synthesizes information such as
  ``presence'' and notify it on the bus, in order to be used by other
  entities.  By proceeding in this way, this scenario should appear by
  itself as a device on the bus, with its address and its schema.  This
  scenario is a kind of virtual device.

\paragraph{User Interfaces.}
  One or many  user interfaces are provided by specific entities connected
  to the xAAL bus.  This can be a real hardware device with a screen and
  buttons; or a microphone which performs voice recognition; or a software
  component that generates Web pages (for instance) to be used by a browser
  on a PC, a connected TV, or whatever; or software that provides a REST API
  for mobile applications (tablets, smartphones), to an external server on
  the cloud for advanced services, to an MQTT server, or to offer features
  for services composition, etc.

  Within a home automation system, there may be one or many user interfaces.




%                                      *
%                                     * *

\section{The xAAL Devices Behavior and xAAL Schemas}\label{schemas}
xAAL devices are described by so-called schemas.  Schemas are documents
specifying attributes, notifications and methods API.


\subsection{Definition of a device}\label{def_device}
A \emph{device} has a \texttt{dev\_type} and an \texttt{address}.

\paragraph{dev\_type:} This references the \emph{schema} and defines the 
  type of the device.  It is hard-coded into the device.
  
  \begin{itemize}
  \item This schema identifier is a string consisting of a pair of words
    separated by a dot. For instance: \texttt{dev\_type:\emph{class}.\emph{variant}}
    \begin{itemize}[label=]
    \item The first word refers to a class of device type (e.g. lamp,
    switch, thermometer, power plug).
    \item The second word refers to a variant of a given class (e.g.  within
      the lighting class one may have an on-off lamp, a lamp with dimmer, an
      RGB lamp, etc.).  The second word may also refer to a schema extension
      proposed by a manufacturer (See  \ref{heritage_schemas}).
    \end{itemize}

  \item The keyword \texttt{``any''} is reserved and acts as a wildcard
    for requests.  So that the identifier \texttt{``any.any''} is reserved and
    refers to all variants of all classes within request messages.  This is a
    \emph{virtual} type.  It is not allowed to defines a schema named
    \texttt{``any.any''}.  No one can claim to be of the type
    \texttt{``any.any''}.  Devices having no dedicated attribute, method or
    notification may use the \emph{concrete} type \texttt{``basic.basic''}.

  \item For example, the pair \texttt{``lamp.any''} means all variants of the
    class \texttt{``lamp''}.  This is also an abstract type.  
    Remember that the concrete type \texttt{``lamp.basic''} is provided to 
    describe basic features common to all lamps.

  \item We reserve a special name: \texttt{``experimental''}, for the
    class, as well as for variants.  This designates concrete types. 
    Associated schema, if written, should not be distributed outside the
    testing platform.  (e.g.  When someone makes a device but has not got a
    standard name for its type.)
  \end{itemize}

\paragraph{Address:} The device ID, unique on the bus.

  \begin{itemize}
  \item This device identifier is a UUID (RFC 4122), a 128-bit random number.
  \item Addresses are self-assigned. There is no naming service anywhere. A
    device does not request any entity to get an address.\\
    Concretely, addresses may be assigned:
    \begin{itemize}[label=]
    \item Either this is hard-coded in the factory.
    \item Auto-generated (random)  at the time of installation.  However, it
      is recommended that this address remain persistent. (i.e.  Please save
      it, if possible, during power breaks.)
    \item There is in principle very high probability of having no collision
      of UUIDs.  However, it is technically possible to verify that a UUID
      is not already used on the bus by a kind of ``Gratuitous ARP'', i.e. 
      by \texttt{is\_alive} requests to ensure that no one else already uses
      this address.
    \item But certainly not assigned by a bus supervisor/coordinator or
      something like that.
    \end{itemize}
  \end{itemize}


\subsection{Definition of a schema, the type of device}\label{ref_schema}
Each device is typed, i.e. described by a schema. 

A xAAL schema is a JSON object with a specific form that must validate given
CDDL rules (\emph{Concise Data Definition Language}, RFC 8610).  See
Appendix \ref{schema.cddl}.

The schema provides a bit of semantics for a device and describes its
capabilities: a list of attributes that describe the operation of the device
in real time (the device announces change of values on the bus), a list of
possible methods on this device (mechanism of request replies), a list of
notifications that it emits spontaneously.

\begin{itemize}
\item A list of \emph{attributes}.  If the value changes, this spontaneously
  generates a notification to the bus.

  Each attribute is defined by:
  \begin{itemize}[topsep=-1ex,parsep=0pt,partopsep=0pt,itemsep=0pt]
  \item A unique name which identifies it within the schema;
  \item A type name relating to the data model section.
  \end{itemize}

\item A list of \emph{methods}. Each method is described by:
  \begin{itemize}[topsep=-1ex,parsep=0pt,partopsep=0pt,itemsep=0pt]
  \item A unique name which identifies it within the schema;
  \item A textual description;
  \item A list of \texttt{``in''} arguments to be filled by peers when
    invoking this method;
  \item A list of \texttt{``out''} arguments returned by the device to
    peers;

    Each argument is defined by:
    \begin{itemize}[label=,topsep=-1ex,parsep=0pt,partopsep=0pt,itemsep=0pt]
    \item A unique name which identifies it within this method definition in
      the schema;
    \item A type name relating to the data model section.
    \end{itemize}

  \item A list of related device's attributes that may be affected by the method
      (e.g. to be refreshed in an HMI after the method call)
  \end{itemize}

\item A list of \emph{notifications}. Each notification is described by:
  \begin{itemize}[topsep=-1ex,parsep=0pt,partopsep=0pt,itemsep=0pt]
  \item A unique name which identifies it within the schema;
  \item A textual description;
  \item A list of \texttt{``out''} arguments included in the notification;
    Each argument is defined by:
    \begin{itemize}[label=,topsep=-1ex,parsep=0pt,partopsep=0pt,itemsep=0pt]
    \item A unique name which identifies it within this notification definition in the schema;
    \item A type name relating to the data model section.
    \end{itemize}
  \end{itemize}

\item A \emph{data model} section, that is to say a list of \emph{data types}
  definitions. For each type name:
  \begin{itemize}[topsep=-1ex,parsep=0pt,partopsep=0pt,itemsep=0pt]
  \item A textual description;
  \item The unit, if any.  This unit should be one of the IANA Sensor
    Measurement Lists (SenML)
    registry.\footnote{\url{https://www.iana.org/assignments/senml/senml.xhtml}}
    \footnote{Note that, contrary to SenML, within the xAAL context, the
    percent symbol \texttt{``\%''} is used for values between 0 and 100, and
    not for values between 0 and 1.} If none is relevant, the unit should be
    one defined by the International Bureau of Weights and Measures.  A
    standard unit allows automatic processing, data computation, or at least
    a consistent way for rendering by HMIs;
  \item A type definition in the form of CDDL rules, for extra processing
    (to dynamically build software skeleton, a generic HMI, etc.).
  \end{itemize}

\end{itemize}

\subsection{Inheritance of schemas} \label{heritage_schemas}
There is a notion of inheritance between schemas.  A schema can extend an
existing schema.  xAAL defines the first three levels of this genealogy:

\begin{enumerate} 

\item A basic generic schema, named \texttt{basic.basic}, common to all
  existing devices in the world, that everyone has to implement.  (See
  \ref{basic_schema}.)

\item A basic schema for every class (e.g.  \emph{lamp.basic},
  \emph{thermometer.basic}, \emph{switch.basic}, etc.).  Such basic schemas
  inherit from the generic schema, and extend it by defining basic
  functionalities shared by all device variants of the corresponding class. 
  (e.g.  lamps basically can do on/off)

\item Advanced schemas for more complex devices, by extending the
  basic schema of the corresponding class, and by defining new
  functionalities.  (e.g.  lamps basically can be turned on and off, but
  some more sophisticated lamps are dimmable, others offer RGB control,
  etc.).  All variants of a class must inherit the basic schema of the
  class.  E.g.  \texttt{lamp.dimmer} extends \texttt{lamp.basic} which
  extends \texttt{basic.basic}

\item Thereafter, manufacturers of home automation equipment will naturally
  define their own schemas among their products range.  However,
  manufacturers must not define their own schemas as level 1 schemas (the
  generic schema is the only one), nor as level 2 schemas (basic class
  schemas).  Schemas from manufacturers are necessarily extensions of
  schemas from level 2 or higher.

  While naming such schemas, manufacturers may use the second word (the
  variant name) of the pointed pair as their own discretion.  This is their
  responsibility to choose a name that may not conflict with existing ones. 
  However, the schema name should refer above all of the functionality of
  the device and means to interact with it; the name should also give an
  idea of the nature of the device.
\end{enumerate}


\paragraph{Definition of the extension process:} Let's consider a first
schema which is extended by a second schema.  The later express differences
with the former.  The extension process produces a new schema.  Extending a
schema has the following meaning:
\begin{itemize}
\item The latter schema may introduce new \emph{attributes}, \emph{methods},
  \emph{notifications}, and associated \emph{data types}.  The result of
  the \emph{extend} operator is a new schema that contains all
  \emph{attributes}, \emph{methods}, \emph{notifications}, \emph{data types}
  of former schema, plus the new ones introduced by the latter schema.

\item The latter schema may overload the definition of some existing objects,
  that is to say \emph{attributes}, \emph{methods}, \emph{notifications},
  and \emph{data types}.  An overloading means that such an object has the
  same name but a different definition.  There is no attempt to merge the
  old and the new object definition.  The resulting schema contains all new
  objects (\emph{attributes}, \emph{methods}, \emph{notifications},
  \emph{data types}) plus old ones that have not been overloaded.
\end{itemize}


\section{The Basic Schema}\label{basic_schema}
This is the basement of every schema.  This is normative.  All other schemas
must inherit from it somehow.  It is named \texttt{basic.basic}.

\paragraph{Attributes}
  \begin{itemize}
    \item \emph{Attributes involved in the protocol}
    \begin{itemize}[label=]
    \item \texttt{dev\_type}: string, the name of the schema to which the
      device obeys;
    \item \texttt{address}: byte string of 16 bytes, a UUID (the address of
      the device);
    \end{itemize}

  \item \emph{Attributes describing the device}
    \begin{itemize}[label=]
    \item \texttt{vendor\_id}: string, the name or identifier of the vendor;
    \item \texttt{product\_id}: string, an identifier of the product
      assigned by the vendor;
    \item \texttt{version}: string, version or revision of the product
      assigned by the vendor;
    \item \texttt{hw\_id}: any type, some hardware identifier of the device
      (e.g.  low-level addresses of the underlying protocol, a pairing code,
      a serial number, or any piece of information that may help to retrieve
      a device within a facility for maintenance);
    \item \texttt{group\_id}: bytestring[16], a UUID shared by all devices
     belonging to the same physical equipment (e.g.  each plug of a
     multi-plug outlet, each thermometer and hydrometer of a weather
     station, etc.);
    \item \texttt{url}: string, the URL of a website with extra 
      information
    \item \texttt{schema}: string, the URL to download the schema file in
      case if the device is of a non-standard \texttt{dev\_type};
    \item \texttt{info}: string, any additional information, if any, about this
      device that should make sense for the end user; (e.g.  on the
      thermometer of a weather station, this may indicate that this is the
      \emph{indoor thermometer} or the \emph{outdoor thermometer}; or on a
      plug belonging to multi-plug outlets, this may indicate the position of
      the plug.)
    \item \texttt{unsupported\_attributes}: array of strings (hopefully
      empty), with names of attributes of the schema that are actually not
      supported by the device for some (bad) reason.
    \item \texttt{unsupported\_methods}: array of strings (hopefully
      empty), with names of methods of the schema that are actually not
      supported by the device for some (bad) reason.
    \item \texttt{unsupported\_notifications}: array of strings (hopefully
      empty), with names of notifications of the schema that are actually
      not supported by the device for some (bad) reason.
    \end{itemize}
 \end{itemize}

  The attributes of the \texttt{basic.basic} schema are mostly considered as
  \emph{internal}, or dedicated to the description of the
  device, and are not likely to change along the life of the device.  Unlike
  attributes of extending schemas, they must not be involved in the
  \texttt{attributes\_change} notification nor in the \texttt{get\_attributes}
  method described below, but via the \texttt{get\_description} method.

\paragraph{Notifications}
  \begin{itemize}
  \item \texttt{alive}: emitted when starting the device and then
    periodically emitted at a rate left to the discretion of the device. 
    The notification message may contain a \texttt{timeout} parameter
    indicating to others when the next \texttt{alive} should arise.

  \item \texttt{attributes\_change}: emitted at every change of one of the
    attributes (except those belonging to \texttt{basic.basic}).  The body
    of the message contains only attributes which changed.

    A schema gives the list of all possible attributes that may appear
    within this notification message.  So, in a given message, some of those
    attributes may be present, some other may not be.  This is normal.

    However, the generic schema defines this method with no \texttt{out} attributes,
    since the default attributes defined above are relating to the
    description of the device, and do not characterize the real-time
    operation of a specific feature.  Schemas extending \texttt{basic.basic}
    may overload the \texttt{attributes\_change} notification according to the
    extra attributes introduced by the extending schema.

  \item \texttt{error}: issued when the device detects a major error or a
    failure.\\
    The notification may contain a \texttt{description} (a textual
    description of the error), and a \texttt{code} (the numeric code of error).

    Remember that xAAL is of the best-effort philosophy.  Therefore, a wrong
    method invocation does not issue \texttt{error} notifications.  (The
    called device does its best with what it received, and possibly change its
    attributes accordingly, but there is no one-to-one dialogue to explain
    mistakes to the caller.) Errors are issued only on major failure of the
    device.

    This is intended to be overridden in the definitions of extending
    schemas.

  \end{itemize}

\noindent
Note: notification messages should be addressed to all (i.e. the target
field is left empty).

\paragraph{Methods}
  \begin{itemize}
  \item \texttt{is\_alive}
    \begin{itemize}[label=]
    \item \texttt{dev\_types} (in): array of \emph{dev\_type} strings, 
      giving names of the schema of devices that should wake up.

    \item One may have:
      \begin{itemize}[label=]
      \item an empty array or \texttt{["any.any"]} to wake up everybody, or
      \item an abstract type, e.g.,\texttt{["lamp.any" ]} to wake up all
        lamps, or
      \item a specific type, e.g.  \texttt{["lamp.basic", "lamp.dimmer"]} to
        wake up just those types of lamp, or
      \item an array of above-mentioned items, e.g.  \texttt{["lamp.dimmer",
        "shutter.basic"]} if we are interested in that, etc.
      \end{itemize}

    \item The target field of an \texttt{is\_alive} request is generally the
        specific address \texttt{(UUID)(0)} reserved for this purpose.  All
        devices should listen to this specific address, and expect a valid
        \texttt{is\_alive} request in the message. This is the discovering
        mechanism proposed by xAAL.

	Alternatively, the target field of an \texttt{is\_alive} may be a
	list of known devices one is waiting for.

	Note that the target field of requests in general is rarely empty.
	Devices should not send broadcast requests.

    \item A \texttt{is\_alive} request must not cause any response message. 
      Instead, recipients(s) of the request must respond as much as possible
      by an \texttt{alive} notification (addressed to all).

    \item This method must not be overloaded by extending schemas.
    \end{itemize}

  \item \texttt{get\_description}
    \begin{itemize}[label=]
    \item \texttt{vendor\_id}, \texttt{product\_id}, \texttt{version},
      \texttt{hw\_id}, \texttt{group\_id}, \texttt{url}, \texttt{info},
      \texttt{unsupported\_attributes},
      \texttt{unsupported\_methods},
      \texttt{unsupported\_notifications}
      (out): see the meaning of the above list of attributes.

    \item This method should not be overloaded with extending schemas.  In
      case if this method is overloaded, the above-listed arguments must
      remain.
    \end{itemize}

  \item \texttt{get\_attributes}
    \begin{itemize}[label=]
    \item \texttt{attributes} (in): array of string, the name of wanted
      attributes.  If the array is empty or if this parameter is absent
      within the request, all attributes should be returned.
  
   \item \texttt{<key values of attributes>} (out): Attributes actually
      returned.  The schema \texttt{basic.basic} defines this method with no
      attributes.  However, this method may be overridden in the definition
      of extending schemas.

    \item It is not mandatory to return all requested attributes.  Peers
      should not make any assumption on this.

    \item Reminder: devices do not return the attributes defined by the
      \texttt{basic.basic} schema, only attributes introduced by extending
      schemas.

    \end{itemize}
  \end{itemize}



%                                      *
%                                     * *


\section{The xAAL Communication Protocol}
xAAL is a distributed system.  Participating entities need to communicate
with each other. 

xAAL messages are carried by UDP multicast packets.  xAAL messages are made
of two layers: (i) a Security Layer with some clear fields mandatory for
transport to receivers, followed by a ciphered payload; and (ii) an
Application Layer which consists of the decrypted payload and containing all
information for participating applications.


\subsection{The Security Layer}
The data of the Security Layer is the payload of the UDP multicast messages 
of the xAAL bus. This is a CBOR array of 5 fields:
\begin{itemize}
\item {[0]}:\emph{version} - Unsigned int, of the value \texttt{7}.
  The version of the protocol.
  Other values should be rejected, the message is ignored.
\item {[1]}:\emph{seconds} - Unsigned int.  First half of the timestamp. 
  The number of seconds since the Epoch (1970-01-01 00:00:00 +0000 UTC).
\item {[2]}:\emph{microseconds} - Unsigned int.  Second half of the
  timestamp.  The number of microseconds since the beginning of above
  second.
\item {[3]}:\emph{targets} - A definite byte string build as the CBOR
  serialization of the array of destination addresses for the message.  Note
  that xAAL device addresses are UUID (see Sec.\ref{schemas}), here encoded
  as definite bytestring[16] without tags.  A xAAL device receiving a
  message should accept it if its own xAAL address is present in the array
  of targets.  An empty target array means a broadcast message.  A target
  field containing an empty byte string is not allowed (the message should
  be ignored).
\item {[4]}:\emph{payload} - A definite byte string which is the ciphered
  \emph{Application Layer} according to version 0.5-r2 principles
  (Poly1305/Chacha20, a symmetric key, a binary nonce build on the timestamp
  of messages, an acceptance window for the timestamp of messages, the
  target field covered by the cryptographic signature).
\end{itemize}

If a message includes other fields in addition to the above mandatory ones,
the message may be accepted, but the extra fields must be ignored.

The above-described CBOR items must have no CBOR tags.

\begin{figure}
\centering
\begin{verbatim}
security_layer = [
  version : 7,
  timestamp_sec: uint,
  timestamp_usec: uint,
  targets : bytes .cbor ([ * (bstr .size 16) ]),
  payload : bstr
]
\end{verbatim}
\caption{CDDL specification of the xAAL Security Layer\label{SecurityLayer}}
\end{figure}

Figure \ref{SecurityLayer} gives the CDDL specification of the xAAL Security
Layer.



\subsection{The Application Layer}
The Application Layer is a CBOR array of size 4 or 5, depending if
there is a \emph{body} field or not:
\begin{itemize}
\item {[0]}:\emph{source} - A definite bytestring[16]; the xAAL address
  (UUID) of the sender of the message.
\item {[1]}:\emph{dev\_type} - A definite string; the schema name of the
  sender (The pair \emph{classe.variant}.)
\item {[2]}:\emph{msg\_type} - An usigned int of value: \texttt{0}:notify,
  \texttt{1}:request, \texttt{2}:reply.
  Other values must be rejected, the message is ignored.
\item {[3]}:\emph{action} - A definite string; the name of the 
  action brought by the message from the list of \emph{methods} and 
  \emph{notifications} described in the considered schema. The considered 
  schema is the one of the sender for notifications and methods replies, and
  the one of the target(s) for methods requests.
\item {[5]}:\emph{body} - An optional field.  If present, it must be a map. 
  Keys of this map are definite strings, the names of parameters associated
  with the corresponding action according to the schema and their value. 
  Depending on data model specified in the schema, values may have CBOR
  tags.  Note that the body may be absent if the schema does not specify
  parameters for the corresponding action. Multiple identical keys are not
  allowed, the message should be ignored.
\end{itemize}

The above-described CBOR items must have no CBOR tags.
However, value items within the \emph{body} may be tagged, according to the
corresponding schema.


\begin{figure}
\centering
\begin{verbatim}
application_layer = [
  source : bstr .size 16,
  dev_type : tstr .regexp "[a-zA-Z][a-zA-Z0-9_-]*\\.[a-zA-Z][a-zA-Z0-9_-]*",
  msg_type : 0..2,
  action : tstr
  ? body : { * ( tstr => any ) }
]
\end{verbatim}
\caption{CDDL specification of the xAAL Application Layer\label{ApplicationLayer}}
\end{figure}

Figure \ref{ApplicationLayer} gives the CDDL specification of the xAAL
Application Layer.


\subsection{The Ciphering Method}
The security of the xAAL bus is ensured by:
\begin{itemize}
\item A symmetric key, pre-shared into all participating devices;
\item Poly1305/Chacha20 as the only cryptographic algorithm, and used
  according to RFC 7905 recommendations (i.e.  with a 96 bits nonce and a
  256 bits key);
\item A binary nonce build as a timestamp since the Epoch
  ( seconds (64 bits) + microseconds (32 bits) );
\item An acceptance window for the timestamp of messages;
\item The list of targets in clear, but covered by the signature;
\item A Security Layer in CBOR, as described above;
\item An Application Layer in CBOR, as described above;
\item The Application Layer is CBOR serialized to produce binary data,
  ciphered into a binary buffer which is placed into the Security Layer as a
  CBOR definite byte string.
\end{itemize}


\paragraph{Notes:}
\begin{itemize}
\item The target field of the Security Layer is a CBOR definite
  byte string.  This is not an array of UUIDs, this is the CBOR
  serialization of an array of UUIDs.  This byte string may be seen as a
  buffer of bytes and can directly be used as the \emph{public additional
  data} for the Poly1305/Chacha20 algorithm, to be covered by the
  cryptographic signature.
\item The binary nonce (96 bits) to be used with Poly1305/Chacha20 is
  composed of the seconds and microseconds (in this order): first a 64 bits
  big-endian unsigned integer (seconds), followed by a 32 bits big-endian
  unsigned integer (microseconds).
\end{itemize}


\paragraph{Example.} 
Figure \ref{xaalmsgs_sec} gives an example of a xAAL message (the Security
Layer): a message to the target
\texttt{8BCC7ED2-A6AC-4D83-A723-6ED3B168C51F}, with a ciphered payload.

Figure \ref{xaalmsgs_app} shows the decoded payload (the Application Layer):
the sender is the device\\
\texttt{1ADFFD0D-67A6-415D-BC11-74C9CCB32EE9},
of type \texttt{thermometer.basic}, which replies about its attribute
\texttt{temperature:18.0}.

The examples use the textual CBOR Diagnostic Notation.

\begin{figure}[H]
\begin{verbatim}
[ 7,
  1572609657,
  519551,
  h'9F508BCC7ED2A6AC4D83A7236ED3B168C51FFF',
  h'BE67602B9DFC0EDA2CD59FA875109954190D11159C6D67B24CA50201EB09
    84FE782F8BCB4259CD38701027184C5959F080DAD013C7A584F44F7EAE52
    BAA212086AC467C6461AC866F5ECC13C2C5EFA4CD71BDE7987CE68D1E8F0' ]
\end{verbatim}
\caption{Example of a xAAL message (\emph{Security Layer})\label{xaalmsgs_sec}}
\end{figure}

\begin{figure}[H]
\begin{verbatim}
[ h'1ADFFD0D67A6415DBC1174C9CCB32EE9', 
  "thermometer.basic",
  2,
  "get_attributes",
  {"temperature": 18.0} ]
\end{verbatim}
\caption{The decrypted payload of a xAAL message (\emph{Application Layer})\label{xaalmsgs_app}}
\end{figure}



%                                      *
%                                     * *




% \cleardoublepage
\clearpage
\appendix

\section{CDDL Rules for xAAL Schemas}\label{schema.cddl}
{\small\begin{verbatim}
; Definition of Schemas for xAAL version 0.7
; Copyright Christophe Lohr IMT Atlantique 2019
; Copying and distribution of this file, with or without modification, are
; permitted in any medium without royalty provided the copyright notice
; and this notice are preserved.  This file is offered as-is, without any
; warranty.


schema = {
  ; The name of the device schema, i.e. the dev_type
  title: dev_type,

  ; A short description in natural language
  description: tstr,

  ; IETF BCB47 language tag of descriptions
  lang: tstr,

  ; URI (rfc3986) pointing to a more comprehensive documentation
  documentation: tstr,

  ; URI (rfc3986) pointing to the original version of this schema
  ; i.e. before any extention process
  ref: tstr,

  ; License of the the original schema file itself
  ? license: tstr,

  ; The schema name which is extended by this one
  ? extends: dev_type,

  ; List of attributes managed by the device
  ? attributes: { + identifier => type_name },
 
  ; Methods supported by the device
  ? methods: { + identifier => method },

  ; Notifications emitted by the device
  ? notifications: { + identifier => notification },

  ; Specifications of data mentionned in the schema
  ; Typically: attributes, parameters of methods and notifications
  ? datamodel: { + identifier => datadef }
}



; Format of the name of a schema in the form "foo.bar"
dev_type = tstr .regexp "[a-zA-Z][a-zA-Z0-9_-]*\\.[a-zA-Z][a-zA-Z0-9_-]*"


; Format of names for attributes, methods and notification
identifier = tstr .regexp "[a-zA-Z][a-zA-Z0-9_-]*"

type_name = identifier


; Definition of a method
method = {
  ; A short description in natural language
  description: tstr,

  ; List of input parameters
  ? in: { * identifier => type_name },

  ; List of output data
  ? out: { * identifier => type_name },

  ; List of device attributes that may be modified while invoking the method
  ? related_attributes: [ * identifier ]
}


; Definition of a notification
notification = {
  ; A short description in natural language
  description: tstr,

  ; List of output data
  out: { * identifier => type_name }
}


; Definition of a data
; Used by device attributes, methods and notifications parameters
datadef = {
  ; A short description in natural language  
  description: tstr,

  ; Unit, according to the IANA Sensor Measurement Lists (SenML) registry
  ? unit: tstr,

  ; Formal descrtiption in CDDL (rfc8610)
  type: tstr
}
\end{verbatim}}


\end{document}
