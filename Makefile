DOC = xAAL-specs

all: ${DOC}.pdf

${DOC}.pdf: ${DOC}.tex 
	latexmk -pdf ${DOC}

clean:
	latexmk -c ${DOC}

proper: clean
	latexmk -C ${DOC}

.PHONY: all clean proper
